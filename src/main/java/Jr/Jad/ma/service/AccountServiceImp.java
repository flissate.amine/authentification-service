package jr.jad.ma.service;

import jr.jad.ma.entity.AppRole;
import jr.jad.ma.entity.AppUser;
import jr.jad.ma.repository.AppRoleRepository;
import jr.jad.ma.repository.AppUserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;


@Slf4j
@Transactional
@Service
public class AccountServiceImp implements AccountService {
    @Autowired
    private AppRoleRepository appRoleRepository;
    @Autowired
    private AppUserRepository appUserRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public void addRole(AppRole appRole) {
        appRoleRepository.save(appRole);
    }

    @Override
    public void addRoleToUser(String userName, String roleName) {
        AppUser appUser = appUserRepository.findByUserName(userName);
        if (appUser == null) {
            log.error("no user found with name {}", userName);
        } else {
            AppRole appRole = appRoleRepository.findByRoleName(roleName);
            if (appRole == null) {
                log.error("no role found with this libelle {}", roleName);
            } else {
                List<AppRole> listRoles = new ArrayList<>();
                listRoles.add(appRole);
                appUser.setAppRoles(listRoles);
            }
        }
    }

    @Override
    public List<AppUser> listUsers() {
        return appUserRepository.findAll();
    }

    @Override
    public List<AppRole> listRoles() {
        return appRoleRepository.findAll();
    }

    @Override
    public void saveUser(AppUser appUser) {
        AppUser checkUser = appUserRepository.findByUserName(appUser.getUserName());
        if (checkUser != null) {
            log.error("this user already exists !");
        }
        AppUser user = new AppUser();
        user.setUserName(appUser.getUserName());
        user.setUserPassword(passwordEncoder.encode(appUser.getUserPassword()));
        try {
            appUserRepository.saveAndFlush(user);
            log.info("operation success : user saved");
        } catch (Exception ex) {
            ex.printStackTrace();
            log.error("something gone wrong , cant save this user");
        }
    }

    @Override
    public AppUser getUserById(String idUser) {
        log.info("incoming request to get user by id : {}", idUser);
        Optional<AppUser> userById = appUserRepository.findById(idUser);
        return userById.orElse(null);
    }

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        AppUser appUser = appUserRepository.findByUserName(userName);

        if (appUser == null) {
            throw new UsernameNotFoundException("User " + userName + " not found");
        }
        Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>() {};
        appUser.getAppRoles().forEach(appRole -> authorities.add(new SimpleGrantedAuthority(appRole.getRoleName())));
        return  User//
                .withUsername(userName)//
                .password(appUser.getUserPassword())//
                .authorities(authorities)//
                .accountExpired(false)//
                .accountLocked(false)//
                .credentialsExpired(false)//
                .disabled(false)//
                .build();
    }
}
