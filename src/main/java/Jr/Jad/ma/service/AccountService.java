package jr.jad.ma.service;

import java.util.List;

import jr.jad.ma.entity.AppRole;
import jr.jad.ma.entity.AppUser;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

@Service
public interface AccountService extends UserDetailsService {

    void addRole(AppRole appRole);

    void addRoleToUser(String userName, String roleName);

    List<AppUser> listUsers();

    List<AppRole> listRoles();

    void saveUser(AppUser junior);

    AppUser getUserById(String idUser);

}
