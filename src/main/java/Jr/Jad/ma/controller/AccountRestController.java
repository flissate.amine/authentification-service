package jr.jad.ma.controller;

import jr.jad.ma.config.JwtProvider;
import jr.jad.ma.entity.*;
import jr.jad.ma.service.AccountService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static jr.jad.ma.config.SecurityConstant.ANONYMIZE_URL;
import static jr.jad.ma.config.SecurityConstant.SECURED_URL;

@Slf4j
@CrossOrigin(value = "*", allowedHeaders = "*")
@RestController
public class AccountRestController {

    @Autowired
    private AccountService accountService;
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private JwtProvider jwtTokenProvider;

    @GetMapping(path = SECURED_URL + "users/all",
            produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public ResponseEntity<List<AppUser>> listUsers() {
        log.info("incoming request to get list of users");
        List<AppUser> appUserList = accountService.listUsers();
        return ResponseEntity.ok().body(appUserList);
    }

    @PostMapping(path = ANONYMIZE_URL + "register",
            consumes = MediaType.APPLICATION_JSON)
    public void saveUser(@RequestBody RegistrationForm registrationForm) {
        List<AppRole> roles = new ArrayList<>();
        AppRole role = new AppRole(1L, "USER");
        roles.add(role);
        if (registrationForm != null) {
            log.info("incoming request to save new  appUser");
            try {
                AppUser appUser = new AppUser();
                appUser.setUserName(registrationForm.getRegistrationName());
                appUser.setUserPassword(registrationForm.getRegistrationPass());
                appUser.setAppRoles(roles);
                accountService.saveUser(appUser);
            } catch (Exception ex) {
                ex.printStackTrace();
                log.error("someething gone wrong cant add this appUser");
            }
        }
        log.error("user info can not be null or empty");
    }

    @GetMapping(path = SECURED_URL + "roles",
            produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public ResponseEntity<List<AppRole>> roles() {
        log.info("incoming request to get list roles");
        List<AppRole> appRoleList = accountService.listRoles();
        return ResponseEntity.ok().body(appRoleList);
    }

    @GetMapping(value = SECURED_URL + "{idUser}", produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public ResponseEntity<AppUser> getUserById(@PathVariable @NotNull String idUser) throws ResourceNotFoundException {
        if (!idUser.isEmpty()) {
            AppUser userById = accountService.getUserById(idUser);
            return ResponseEntity.ok().body(userById);
        }
        log.error("id user cant be null or empty");
        return null;
    }

    @PostMapping(value = ANONYMIZE_URL + "login",
    consumes = MediaType.APPLICATION_JSON,
    produces = MediaType.APPLICATION_JSON)
    public ResponseEntity<?> login(@RequestBody UserRequestDTO appUser) {
        log.info("incoming request to sign in user {}", appUser.toString());
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(appUser.getUserName(), appUser.getUserPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        UserDetails user = accountService.loadUserByUsername(appUser.getUserName());

        String token = jwtTokenProvider.generateToken(user);
        log.info("generated token : {}", token);
        String[] claimsFromToken = jwtTokenProvider.getClaimsFromToken(token);
        log.info("list of claims : {}",Arrays.toString(claimsFromToken));
        ResponseUserDto responseUserDto = new ResponseUserDto(user.getUsername(), token, claimsFromToken, user.isAccountNonLocked(), user.isEnabled());
        log.info(responseUserDto.toString());
        return ResponseEntity.ok().body(responseUserDto);
    }
}

