package jr.jad.ma.repository;

import jr.jad.ma.entity.AppUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface AppUserRepository extends JpaRepository<AppUser, String> {
   AppUser findByUserName(String userName);
}
