package jr.jad.ma.config;

import org.springframework.http.HttpStatus;

import static jr.jad.ma.config.SecurityConstant.ACCESS_DENIED_MSSG;

public class CustomException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    private String message = ACCESS_DENIED_MSSG;
    private final HttpStatus httpStatus;

    public CustomException(HttpStatus httpStatus, String message) {
        this.message = message;
        this.httpStatus = httpStatus;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public int getHttpStatus() {
        return httpStatus.value();
    }

}
