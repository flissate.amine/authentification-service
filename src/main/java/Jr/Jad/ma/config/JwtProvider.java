package jr.jad.ma.config;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import jr.jad.ma.service.AccountService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static com.auth0.jwt.algorithms.Algorithm.HMAC256;
import static java.util.Arrays.stream;
import static jr.jad.ma.config.SecurityConstant.*;

@Component
public class JwtProvider {
    @Autowired
    private AccountService accountService;

    @Value("jwt.secret")
    private String secret;

    public String generateToken(UserDetails userPrincipal) {
        String[] claims = getClaimsFromUSer(userPrincipal);
        return JWT.create()
                .withIssuer(ISSUER)
                .withIssuedAt(new Date())
                .withArrayClaim(AUTHORITIES, claims)
                .withAudience(AUDIENCE)
                .withExpiresAt(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
                .withSubject(userPrincipal.getUsername())
                .sign(HMAC256(secret.getBytes()));

    }

    private String[] getClaimsFromUSer(UserDetails userPrincipal) {
        List<String> authorities = new ArrayList<>();
        for (GrantedAuthority authority : userPrincipal.getAuthorities()) {
            authorities.add(authority.getAuthority());
        }
        return authorities.toArray(new String[0]);
    }

    public List<GrantedAuthority> getAuthorities(String token) {
        String[] claims = getClaimsFromToken(token);
        return stream(claims).map(SimpleGrantedAuthority::new).collect(Collectors.toList());
    }

    public String[] getClaimsFromToken(String token) {
        JWTVerifier jwtVerifier = getJwtVerifier();
        return jwtVerifier.verify(token).getClaim(AUTHORITIES).asArray(String.class);
    }

    private JWTVerifier getJwtVerifier() {
        JWTVerifier jwtVerifier;
        try {
            Algorithm algorithm = HMAC256(secret);
            jwtVerifier = JWT.require(algorithm).withIssuer(ISSUER).build();
        } catch (Exception e) {
            throw new JWTVerificationException("cannot verify token");
        }
        return jwtVerifier;
    }

    public Authentication getAuthentication(String username, List<GrantedAuthority> authorities, HttpServletRequest request) {
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(username, null, authorities);
        authenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
        return authenticationToken;
    }

    public boolean isTokenValid(String username, String token) {
        JWTVerifier jwtVerifier = getJwtVerifier();
        return StringUtils.isNotEmpty(username) && isTokenExpired(jwtVerifier, token);

    }

    private boolean isTokenExpired(JWTVerifier jwtVerifier, String token) {
        Date expirationTokenDate = jwtVerifier.verify(token).getExpiresAt();
        return expirationTokenDate.before(new Date());
    }

    public String getSubject(String token) {
        JWTVerifier jwtVerifier = getJwtVerifier();
        return jwtVerifier.verify(token).getSubject();
    }
}
