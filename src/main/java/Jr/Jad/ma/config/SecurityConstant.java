package jr.jad.ma.config;

public class SecurityConstant {

    public static final String FREE_ACCESS_API = "**/api/**";
    public static final String SECURED_API = "**/auth/**";
    public static final String HEADER_NAME = "Authorization";
    public static final String AUTHORITIES = "Authorities";
    public static final String OPTIONS_HTTP_METHODES = "OPTIONS";
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String AUDIENCE = "ADMIN ";
    public static final String ISSUER = "JR_COMPAGNY ";
    public static final String ACCESS_DENIED_MSSG = "vous n'etes pas autorisé a acceder ";
    public static final String ERRONED_PASSWRD = "Incorrect password ";
    public static final Long EXPIRATION_TIME = 1000L * 60 * 30;
    public static final String SECURED_URL = "/auth/";
    public static final String ANONYMIZE_URL = "/api/";
}
