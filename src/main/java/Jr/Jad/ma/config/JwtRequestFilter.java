package jr.jad.ma.config;

import jr.jad.ma.service.AccountService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

import static jr.jad.ma.config.SecurityConstant.*;

@Slf4j
@Component
public class JwtRequestFilter extends OncePerRequestFilter {
    @Autowired
    JwtProvider jwtTokenProvider;
    @Autowired
    AccountService accountService;

    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response,
                                    FilterChain filterChain)
            throws ServletException, IOException {

        if (request.getRequestURI().equalsIgnoreCase(OPTIONS_HTTP_METHODES)) {
            response.setStatus(HttpStatus.OK.value());
        } else {
            String authorisationHeader = request.getHeader(HEADER_NAME);
            if (authorisationHeader != null && authorisationHeader.startsWith(TOKEN_PREFIX)) {
                String jwt = authorisationHeader.substring(7);
                String username;
                try {
                    username = jwtTokenProvider.getSubject(jwt);
                } catch (Exception e) {
                    throw new IllegalArgumentException(e);
                }

                if (username != null && SecurityContextHolder.getContext().getAuthentication() == null) {
                    UserDetails userDetails = accountService.loadUserByUsername(username);
                    List<GrantedAuthority> authorities = jwtTokenProvider.getAuthorities(jwt);
                    if (jwtTokenProvider.isTokenValid(username, jwt)) {
                        jwtTokenProvider.getAuthentication(userDetails.getUsername(), authorities, request);
                    }
                }
                filterChain.doFilter(request, response);
            }
            filterChain.doFilter(request, response);
        }
    }
}
