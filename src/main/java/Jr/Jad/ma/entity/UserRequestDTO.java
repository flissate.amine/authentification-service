package jr.jad.ma.entity;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Data
@Getter
@Setter
@ToString
public class UserRequestDTO {
    private String userName;
    private String userPassword;

    public UserRequestDTO(String userName, String userPassword) {
        this.userName = userName;
        this.userPassword = userPassword;
    }

    public UserRequestDTO() {
    }
}
