package jr.jad.ma.entity;

import lombok.*;
import org.hibernate.Hibernate;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;
import java.util.UUID;

@Entity
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@AllArgsConstructor
@Table(name = "USER")
public class AppUser {
	@Id
	@Basic(optional = false)
	@Column(name = "ID_USER",unique=true, nullable = false)
	private String uuid = UUID.randomUUID().toString();
	private String userName;
	private String userPassword;
	@ManyToMany(fetch = FetchType.EAGER)
	private Collection<AppRole> appRoles= new ArrayList<>();
	private String email;
	private String userProfilePicture;
	private boolean isActive;
	private boolean isLocked;
	private String joinDate;


	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
		AppUser appUser = (AppUser) o;
		return uuid != null && Objects.equals(uuid, appUser.uuid);
	}

	@Override
	public int hashCode() {
		return getClass().hashCode();
	}
}
