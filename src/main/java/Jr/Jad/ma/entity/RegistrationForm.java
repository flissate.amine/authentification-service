package jr.jad.ma.entity;

import lombok.Data;

@Data
public class RegistrationForm {
    private String registrationName;
    private String registrationPass;
    private String email;
    private int phoneTel;

    public RegistrationForm() {
    }

    public RegistrationForm(String registrationName, String registrationPass, String email, int phoneTel) {
        this.registrationName = registrationName;
        this.registrationPass = registrationPass;
        this.email = email;
        this.phoneTel = phoneTel;
    }

    public String getRegistrationName() {
        return registrationName;
    }

    public void setRegistrationName(String registrationName) {
        this.registrationName = registrationName;
    }

    public String getRegistrationPass() {
        return registrationPass;
    }

    public void setRegistrationPass(String registrationPass) {
        this.registrationPass = registrationPass;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getPhoneTel() {
        return phoneTel;
    }

    public void setPhoneTel(int phoneTel) {
        this.phoneTel = phoneTel;
    }
}
