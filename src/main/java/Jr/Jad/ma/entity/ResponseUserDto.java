package jr.jad.ma.entity;

import lombok.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ResponseUserDto {
    private String userName;
    private String token;
    private String[] claimsFromToken;
    private boolean accountNonLocked;
    private boolean enabled;
}
